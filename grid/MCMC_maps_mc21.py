import TopExamples.grid

TopExamples.grid.Add("TT_PP8_dilep").datasets = [
  "mc21_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e8453_s3873_r13829_p5226",
]

TopExamples.grid.Add("TT_PH7_dilep").datasets = [
  "mc21_13p6TeV.601415.PhH7EG_A14_ttbar_hdamp258p75_Dilep.deriv.DAOD_PHYS.e8472_e8455_s3873_s3874_r13829_r13831_p5226",
]

TopExamples.grid.Add("TT_PSh_dilep").datasets = [
  "mc21_13p6TeV.700660.Sh_2212_ttbar_dilepton_maxHTavrgTopPT.deriv.DAOD_PHYS.e8462_e8455_s3873_s3874_r13829_r13831_p5226",
]


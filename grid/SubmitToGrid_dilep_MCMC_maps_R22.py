#!/usr/bin/env python

# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
import TopExamples.grid
#import DerivationTags
#import Data_rel21
import MCMC_maps_mc21

config = TopExamples.grid.Config()
config.code          = 'top-xaod'
config.settingsFile  = 'MCMC_maps-xsec-dilep-mc21.txt'
config.gridUsername  = 'gguerrie'
config.suffix        = 'MCMC-maps-ttbar-Sh'
config.excludedSites = ''
config.maxNFilesPerJob = '10'
config.noSubmit      = False # set to True if you just want to test the submission
config.CMake         = True # need to set to True for CMake-based releases (release 21)
config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
config.destSE        = '' #This is the default (anywhere), or try e.g. 'UKI-SOUTHGRID-BHAM-HEP_LOCALGROUPDISK'
config.reuseTarBall = False
#config.otherOptions = '--extFile=prw_allDsids.root'


###Edit these lines if you don't want to run everything!
names = [
#    'TT_PH7_dilep',
#    'TT_PP8_dilep',
    'TT_PSh_dilep',
]

samples = TopExamples.grid.Samples(names)
TopExamples.grid.submit(config, samples)


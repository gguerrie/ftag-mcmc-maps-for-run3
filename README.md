# FTag MCMC Maps for Run3

## Getting started
Run the `setup.sh` script to initialize the environment.


## Configs to run on the grid
In the `grid` folder are placed the steering files for the dataset production:

1. `MCMC_maps-xsec-dilep-mc21.txt`: AnalysisTop config for the production of the JetEfficiencyPlots
2. `MCMC_maps_mc21.py`: python list with the set of relevant rucio containers
3. `SubmitToGrid_dilep_MCMC_maps_R22.py`: steering file for the submission. Edit this file with the relavant information and submit the jobs by running 
```
python SubmitToGrid_dilep_MCMC_maps_R22.py
```


## Produce the ratio plots
Create a folder called `images` into which save the plots:
```
mkdir images
```
Update the dictionary in the `compare_maps.py` script with the paths to the `.root` files corresponding to `Pythia8`, `Herwig7` and `Sherpa`. 
Then, run the script:
```
python compare_maps.py -i <sample>
```
where the option `-i` can be `[H7,SH]`, respectively for `Herwig7` and `Sherpa`. The tagger is set as `dl1dv01` by default.
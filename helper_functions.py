#!/usr/bin/env python
import sys
import ROOT
from itertools import product
import os

texlist = []
box = []
def draw_tex(flav,tagger):
    
    p = ROOT.TPad("p","p",0.,0.,1.,1.)
    p.SetFillStyle(0)
    p.Draw("same")
    p.cd()
   
    box.append(ROOT.TBox(0.13,0.74,0.42,0.88))
    #box.SetLineColor("kRed")
    box[-1].SetFillColor(0)
    box[-1].Draw('same')

    texlist.append( ROOT.TLatex(0.14,0.84,"ATLAS") )
    texlist[-1].SetNDC();
    texlist[-1].SetTextFont(72);
    texlist[-1].SetLineWidth(2);
    texlist[-1].SetTextSize(0.040);
    texlist[-1].Draw('same');

    texlist.append( ROOT.TLatex(0.22,0.84,"Internal Simulation") )
    texlist[-1].SetNDC();
    texlist[-1].SetTextFont(42);
    texlist[-1].SetLineWidth(2);
    texlist[-1].SetTextSize(0.040);
    texlist[-1].Draw("same");

    texlist.append( ROOT.TLatex(0.14,0.8,"#sqrt{s} = 13.6 TeV") )
    texlist[-1].SetNDC();
    texlist[-1].SetTextFont(12);
    texlist[-1].SetLineWidth(1);
    texlist[-1].SetTextSize(0.035);
    texlist[-1].Draw('same');

    texlist.append( ROOT.TLatex(0.14,0.76,flav+ '    ' +tagger) )
    texlist[-1].SetNDC();
    texlist[-1].SetTextFont(42);
    texlist[-1].SetTextSize(0.025);
    texlist[-1].SetLineWidth(1);
    texlist[-1].Draw('same');

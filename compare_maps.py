#!/usr/bin/env python
import ROOT
import sys
import argparse
from array import array
import helper_functions


parser = argparse.ArgumentParser(description='MCMC maps for ftag group')
parser.add_argument( '-i', '--sample', default="H7" )
parser.add_argument( '-t', '--tagger',  default="dl1dv01" )


args         = parser.parse_args()
sample = args.sample
tagger  = args.tagger
 
ROOT.gStyle.SetOptStat(0)

tag_list_v0 = ["DL1dv00_FixedCutBEff_77_AntiKt4EMPFlowJets_B_pass","DL1dv00_FixedCutBEff_77_AntiKt4EMPFlowJets_C_pass","DL1dv00_FixedCutBEff_77_AntiKt4EMPFlowJets_L_pass"]
tag_list_v1 = ["DL1dv01_FixedCutBEff_77_AntiKt4EMPFlowJets_B_pass","DL1dv01_FixedCutBEff_77_AntiKt4EMPFlowJets_C_pass","DL1dv01_FixedCutBEff_77_AntiKt4EMPFlowJets_L_pass"]

total_list = ["B_total_AntiKt4EMPFlowJets","C_total_AntiKt4EMPFlowJets","L_total_AntiKt4EMPFlowJets"]

samples = {
  "PP8v00": "/eos/infnts/atlas/gguerrie/13p6_TeV/ntuples/MCMC_maps_ftag_AUG_2022/dl1dv00/user.gguerrie.601230.PhPy8EG.DAOD_PHYS.e8453_s3873_r13829_p5226.MCMC-maps-ttbar-PP8_output_root/user.gguerrie.30117565.output.root",
  "H7v00" : "/eos/infnts/atlas/gguerrie/13p6_TeV/ntuples/MCMC_maps_ftag_AUG_2022/dl1dv00/user.gguerrie.601415.PhH7EG.DAOD_PHYS.e8472_e8455_s3873_s3874_r13829_r13831_p5226_tid30106035_00.MCMC-maps-ttbar-He7_output_root/user.gguerrie.30117569.output.root",
  "SHv00" : "/eos/infnts/atlas/gguerrie/13p6_TeV/ntuples/MCMC_maps_ftag_AUG_2022/dl1dv00/user.gguerrie.700660.Sh.DAOD_PHYS.e8462_e8455_s3873_s3874_r13829_r13831_p5226_tid30095965_00.MCMC-maps-ttbar-Sh_output_root/user.gguerrie.30117572.output.root",
  "PP8v01": "/eos/infnts/atlas/gguerrie/13p6_TeV/ntuples/MCMC_maps_ftag_AUG_2022/dl1dv01/user.gguerrie.601230.PhPy8EG.DAOD_PHYS.e8453_s3873_r13829_p5226.MCMC-maps-ttbar-P8_v01_v1_output_root/user.gguerrie.30164370.output.root",
  "H7v01" : "/eos/infnts/atlas/gguerrie/13p6_TeV/ntuples/MCMC_maps_ftag_AUG_2022/dl1dv01/user.gguerrie.601415.PhH7EG.DAOD_PHYS.e8472_e8455_s3873_s3874_r13829_r13831_p5226.MCMC-maps-ttbar-H7_v01_v1_output_root/user.gguerrie.30164450.output.root",
  "SHv01" : "/eos/infnts/atlas/gguerrie/13p6_TeV/ntuples/MCMC_maps_ftag_AUG_2022/dl1dv01/user.gguerrie.700660.Sh.DAOD_PHYS.e8462_e8455_s3873_s3874_r13829_r13831_p5226.MCMC-maps-ttbar-SH_v01_v1_output_root/user.gguerrie.30164427.output.root",
}

if tagger=="dl1dv01":
    pathMC1 = str(samples["PP8v01"])
    pathMC2 = str(samples[sample + "v01"])
    tag_list = tag_list_v1
else:
    pathMC1 = samples["PP8v00"]
    pathMC2 = samples[sample + "v00"]
    tag_list = tag_list_v0

fileMC1 = ROOT.TFile.Open(pathMC1)
fileMC2 = ROOT.TFile.Open(pathMC2)

for idx, btagger in enumerate(tag_list):
    print("\n\n\nbtagger chain = "+btagger)


    if not fileMC1 :
        print(" Failed to get MC1 file ")
        sys.exit (1)

    if not fileMC2 :
        print(" Failed to get MC2 file ")
        sys.exit (1)

    #Getting histograms
    dirMC_name = "ftag/JetFtagEffPlots/"+btagger
    temp_MC1_btag = fileMC1.Get(dirMC_name)
    temp_MC2_btag = fileMC2.Get(dirMC_name)

    # Getting total histograms
    dirMC_name = "ftag/JetFtagEffPlots/"+total_list[idx]
    temp_MC1_btag_total = fileMC1.Get(dirMC_name)
    temp_MC2_btag_total = fileMC2.Get(dirMC_name)


    if idx==0:
        ptbinning = [20.0, 30.0, 40.0, 60.0, 85.0, 110.0, 140.0, 175.0, 250.0, 600.0]
        etabinning = [0.0, 2.5]
    elif idx==1:
        ptbinning = [20.0, 40.0, 65.0, 140.0, 250.0]
        etabinning = [0.0, 2.5]
    else:
        ptbinning = [20.0, 50.0, 100.0, 150.0, 300.0]
        etabinning = [0.0, 2.5]

    # Booking passed histos
    MC1_btag = ROOT.TH2F(
    "MC1_btag",
    "MC1_btag",
    len(ptbinning) - 1,
    array("f", ptbinning),
    len(etabinning) - 1,
    array("f", etabinning),
    )

    MC2_btag = ROOT.TH2F(
    "MC2_btag",
    "MC2_btag",
    len(ptbinning) - 1,
    array("f", ptbinning),
    len(etabinning) - 1,
    array("f", etabinning),
    )

    # Booking total histos
    MC1_btag_total = ROOT.TH2F(
    "MC1_btag",
    "MC1_btag",
    len(ptbinning) - 1,
    array("f", ptbinning),
    len(etabinning) - 1,
    array("f", etabinning),
    )

    MC2_btag_total = ROOT.TH2F(
    "MC2_btag",
    "MC2_btag",
    len(ptbinning) - 1,
    array("f", ptbinning),
    len(etabinning) - 1,
    array("f", etabinning),
    )

    histnames = ["MC1_btag","MC2_btag","MC1_btag_total","MC2_btag_total"]

    for hnew in histnames:
        
        hist = locals()["temp_"+hnew]
        
        xaxis = hist.GetXaxis()
        yaxis = hist.GetYaxis()

        for j in range(1, yaxis.GetNbins() + 1):
            for i in range(1, xaxis.GetNbins() + 1):

                ptval = xaxis.GetBinCenter(i)
                etaval = yaxis.GetBinCenter(j)

                if hist.GetBinContent(i, j) < 0:
                    continue

                if (
                    ptbinning[0] <= ptval < ptbinning[-1]
                    and etabinning[0] <= etaval < etabinning[-1]
                ):
                    locals()[hnew].Fill(ptval, etaval, hist.GetBinContent(i, j))


    MC1_btag.Divide(MC1_btag_total)
    MC2_btag.Divide(MC2_btag_total)

# Scaling section
#####################################################################################
    #MC1_btag.Scale(1. /  MC1_btag.GetMaximum())
    #MC1_btag.Scale(1. /  MC1_btag.Integral())
    #print("MC1_btag integral: " + str(MC1_btag.Integral()))
    #MC2_btag.Scale(1. /  MC2_btag.GetMaximum())
    #MC2_btag.Scale(1. /  MC2_btag.Integral())
    #print("MC2_btag integral: " + str(MC2_btag.Integral()))
#####################################################################################


# Plotting histos and creating ratio plots (h3)
    c2 = ROOT.TCanvas("c2", "c2", 1280, 760)
    gPad = c2.cd(1)
    gPad.SetLogx()
    MC1_btag.SetTitle("P8  " + btagger)
    MC1_btag.GetXaxis().SetTitle("jet p_{T} [GeV]")
    MC1_btag.GetYaxis().SetTitle("jet |#eta|")
    MC1_btag.Draw("COLZ")
    helper_functions.draw_tex(total_list[idx].replace('_total', ''),tagger)
    
    # Draw separate components
    # c2 = ROOT.TCanvas("c2", "c2", 1000, 1000)
    # pad1 = ROOT.TPad("pad2","pad2",0,0.5,1,1)
    # pad1.Draw()
    # pad1.cd()
    # histD_pt = temp_MC1_btag_total.ProjectionX()
    # histD_pt.GetXaxis().SetRangeUser(0., 400)
    # histD_pt.SetTitle("P8   "+ total_list[idx].replace('_total', '') + "_total")
    # histD_pt.Draw()
    # c2.cd()
    # pad2 = ROOT.TPad("pad2","pad2",0,0,1,0.45)
    # pad2.Draw()
    # pad2.cd()
    # histD_eta = temp_MC1_btag_total.ProjectionY()
    # histD_eta.GetXaxis().SetRangeUser(0., 2.5)
    # histD_eta.SetTitle("P8   "+ total_list[idx].replace('_total', '') + "_total")
    # histD_eta.Draw()
#####################################################################################
    c2.Print("images/" + "P8_" + btagger + ".png")
    c2.Print("images/" + "P8_" + btagger + ".pdf")
    c2.Close()
#####################################################################################
    c3 = ROOT.TCanvas("c3","c3",1280, 760)
    c3.cd()
    gPad = c3.cd(1)
    gPad.SetLogx()
    MC2_btag.SetTitle(sample+ "  " + btagger)
    MC2_btag.GetXaxis().SetTitle("jet p_{T} [GeV]")
    MC2_btag.GetYaxis().SetTitle("jet |#eta|")
    MC2_btag.Draw("COLZ")
    helper_functions.draw_tex(total_list[idx].replace('_total', ''),tagger)
    c3.Print("images/" + sample + "_" + btagger + ".png")
    c3.Print("images/" + sample + "_" + btagger + ".pdf")
    c3.Close()
#####################################################################################
    c1 = ROOT.TCanvas("c1","c1",1280, 760)
    c1.cd()
    gPad = c1.cd(1)
    gPad.SetLogx()
    h3 = MC2_btag
    h3.SetTitle("Ratio "+ sample + "/P8" + "  " + btagger)
    h3.Divide(MC1_btag)
    h3.GetXaxis().SetTitle("jet p_{T} [GeV]")
    h3.GetYaxis().SetTitle("jet |#eta|")
    h3.Draw("COLZ")
    helper_functions.draw_tex(total_list[idx].replace('_total', ''),tagger)
    plotFileName = "ratio_" + sample + "_P8_" + btagger + ".png"
    plotFileName_pdf = "ratio_" + sample + "_P8_" + btagger + ".pdf"
    c1.Print("images/" + plotFileName)
    c1.Print("images/" + plotFileName_pdf)
    c1.Close()
#####################################################################################
    del MC1_btag,MC2_btag,MC1_btag_total,MC2_btag_total

fileMC1.Close()
fileMC2.Close()